# Sorsana
![Demo Animation](/doc/demo.gif)
Sorsana is a cross-platform application that contains curated, fun and thought-provoking questions to help people spark up interesting conversations in their social gatherings with friends/families, date nights, going-outs, icebreakers, and networking events; especially when there is nothing left to talk about.

## Requirements
- <a href="https://nodejs.org/en/download/" target="_blank">Node.js LTS</a>

## Instructions
1. Setup (only once): `git clone https://gitlab.com/furkanunluturk/sorsana.git && cd sorsana && npm i`
2. Start: `expo start --web`

## More
See more in [/docs](https://gitlab.com/furkanunluturk/sorsana/-/tree/master/doc).

## Resources
- https://reactnative.dev/docs/stylesheet

## Problems along the way
- https://docs.expo.dev/workflow/run-on-device/ (expo start --tunnel)
- https://justinnoel.dev/2020/12/08/react-native-urlsearchparams-error-not-implemented/
- https://docs.expo.dev/workflow/debugging/#native-debugging (expo prebuild -p android)

## Notes
- "distribution": "internal" means apk
- eas build -p android --profile preview
- use 'expo update' to update your project (together with all the packages)
- while 'npm update' command ONLY upgrades the packages (hence modifies **package-lock.json**) according to package.json, 'npm i <package_name>' ALSO modifies **package.json** by overwriting the new version code.
- for "@supabase/supabase-js": "^1.35.3" :  'npm i @supabase/supabase-js' will upgrade to 1.35.7 (last minor version)
- for "@supabase/supabase-js": "*"       :  'npm i @supabase/supabase-js' will upgrade to 2.0.5  (last major version)