# Sorsana
## Links

### Main Project
- <https://gitlab.com/furkanunluturk/sorsana>
- [Issue board (Agile board)](https://gitlab.com/furkanunluturk/sorsana/-/boards) & [Milestones (Sprints)](https://gitlab.com/furkanunluturk/sorsana/-/milestones)
- Prototype: <https://www.figma.com/proto/Imjfrzlfbi2te3LgVSDYjB/Sorsana>

### Subproject
- <https://github.com/frknltrk/react-native-swipeable-cards>
- [Project plan](https://github.com/frknltrk/react-native-swipeable-cards/projects)

## Introduction
I prefered to use GitLab instead of Trello for being simple and especially suitable for software projects.
![Agile Plan](agile-board.jpg)

We should be agile, so we should not waste time with categorization of issues in any kind of way and we don't want a nested structure of issues. So I came up with;
![Column Hierarchy](in-progress.jpg)

## Terminology
- sorsana: The name of my project.
- rnsc (react-native-swipeable-cards):  Subproject. The name of the package (dependency) I make use of in my project.

## Project Structure:
- [Sprint1: sorsana](https://gitlab.com/furkanunluturk/sorsana/-/milestones/1)
- [Sprint2: sorsana](https://gitlab.com/furkanunluturk/sorsana/-/milestones/2)
  - [Sprint1: rnsc](https://github.com/frknltrk/react-native-swipeable-cards/projects/2)
  - [Sprint2: rnsc](https://github.com/frknltrk/react-native-swipeable-cards/projects/1)
- [Sprint3: sorsana](https://gitlab.com/furkanunluturk/sorsana/-/milestones/3)

## Project Overview

### First Sprint
I've conducted a thorough research on how to set up the project.

### Second Sprint
Now there was a task (e.g., #10) at hand. As I was working on it, some other issues (e.g., #21) came up. I had to deal with it separately. And this is where the idea of a subproject came in.

#### Subproject
I wanted the package to be tailored to the specific needs of the project so I customized it. I decided to conduct this workflow under a separate and individual repo (project) named [react-native-swipeable-cards](https://github.com/frknltrk/react-native-swipeable-cards).
In the first sprint I just updated the project by fixing the problem of deprecated uses in the codebase.
In the 2nd one, I customized the package by adding new features and removing unnecessary ones for my project.
![Subproject Agile Board](sp-board.jpg)

### Mockup
I've -literally- spent hours to come up with a decent [prototype](https://www.figma.com/proto/Imjfrzlfbi2te3LgVSDYjB/Sorsana) using standard components. It's not complete yet.

![Screen1](proto-1.jpg)
![Screen2](proto-2.jpg)
