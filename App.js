// https://github.com/supabase/supabase-js/blob/master/example/next-todo/components/TodoList.js

import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import SwipeCards from "react-native-swipeable-card-deck";
import { supabase } from "./api";

function Card({ data }) {
  return (
    <View style={styles.card}>
      <Text style={styles.cardsText}>{data.text}</Text>
    </View>
  );
}

function StatusCard({ text }) {
  return (
    <View>
      <Text style={styles.cardsText}>{text}</Text>
    </View>
  );
}

export default function App() {
  const [cards, setCards] = useState();

  useEffect(() => {
  	setTimeout(() => {
  		getAllQs();
  	}, 1000);
  }, []);

  const getAllQs = async () => {
  	let { error, data } = await supabase.from("randomized_questions").select("text");
  	if (error) {
  		console.log(error.message);
  		return;
  	}
  	let welcome_message = "Sorsana'ya hoş geldin. Sonraki soruya geçmek için sola at, önceki için sağa.";
  	setCards([{text: welcome_message}].concat(data));
  }

  const getRandomQ = async () => {
  	let { error, data } = await supabase.from("randomized_questions").select("text").limit(1).single();
  	if (error) {
  		console.log(error.message);
  		return
  	}
  	return data
  }

  function handleLeft() {
  	//getRandomQ() if (currentCardIndex == cards.length - 1)
  	//console.log(`Next`);
    return true;
  }
  
  function handleRight() {
  	//console.log(`Previous`);
  	return true;
  }

  return (
    <View style={styles.container}>
      {cards ? (
        <SwipeCards
          cards={cards}
          renderCard={(cardData) => <Card data={cardData} />}
          keyExtractor={(cardData) => String(cardData.text)}
          loop={true}
          renderNoMoreCards={() => <StatusCard text="Bitti." />}
          hasUpAction={false}
          actions={{
            left: { show:false, onAction: handleLeft },
            right: { show:false, onAction: handleRight },
          }}
        />
      ) : (
        <StatusCard text="Yükleniyor..." />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#161616",
    alignItems: "center",
    justifyContent: "center"
  },
  card: {
    justifyContent: "center",
    alignItems: "center",
    width: "90vh",
    height: "100vh",
    backgroundColor: "grey",
    userSelect: "none",
    padding: 24
  },
  cardsText: {
    fontSize: 22,
    color: "white",
  },
});
