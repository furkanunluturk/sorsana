import AsyncStorage from '@react-native-async-storage/async-storage';
import { createClient } from '@supabase/supabase-js';

const supabaseUrl = "https://yoathwogccbxeowxbyqo.supabase.co";
const supabaseAnonKey = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InlvYXRod29nY2NieGVvd3hieXFvIiwicm9sZSI6ImFub24iLCJpYXQiOjE2NTM3NTUxMzEsImV4cCI6MTk2OTMzMTEzMX0.HLMletVvMCNgR1dZQEMMCPjBlcPiSwD0-pE7lDa0NIE";

export const supabase = createClient(supabaseUrl, supabaseAnonKey, {
  auth: {
    storage: AsyncStorage,
    autoRefreshToken: true,
    persistSession: true,
    detectSessionInUrl: false,
  },
});